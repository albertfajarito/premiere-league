<?php

namespace Tests\Feature;

use App\Events\PlayerDataFetched;
use App\Jobs\FetchPlayersData;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class PlayerTest extends TestCase
{

    public function testPlayerDataFetchingJobQueued()
    {
        $this->withoutEvents();

        Queue::fake();

        Artisan::call('players:run-fetch-players-data-job');

        Queue::assertPushed(FetchPlayersData::class);
    }

    public function testFetchData()
    {
        $this->withoutExceptionHandling();

        $factory = new \App\Library\PlayerDatasources\Datasources\DatasourceFactory();

        $factory->create()->each(function($datasource){
           $data = $datasource->getData();
           $this->assertFalse($datasource->hasErrors());
           $this->assertTrue(is_array($data));
           $this->assertTrue(count($data) > 0);
        });
    }

    public function testSavePlayerDataJobTriggered()
    {
        $this->withoutExceptionHandling();

        Event::fake();

        Artisan::call('players:run-fetch-players-data-job');
        Event::assertDispatched(PlayerDataFetched::class, function($e){
            return is_array($e->data) && count($e->data) > 0;
        });
    }

    public function testFetchPlayerByIdEndpoint()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/api/player/60');

        $response->assertStatus(200);
    }

    public function testFetchAllPlayerEndpoint()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/api/players');

        $response->assertStatus(200);
    }
}
