<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FantasyPremierleague extends Model
{
    //
    protected $fillable = [
      'player_id',
      'data'
    ];
}
