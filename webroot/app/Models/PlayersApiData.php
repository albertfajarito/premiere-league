<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayersApiData extends Model
{
    //
    protected $fillable = [
        'player_id',
        'data'
    ];
}
