<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FetchedPlayerData extends Model
{
    //
    protected $fillable = [
        'source',
        'data'
    ];
}
