<?php
/**
 * Created by PhpStorm.
 * User: albert
 * Date: 9/13/19
 * Time: 8:02 AM
 */

namespace App\Library\PlayerDatasources\HttpClient;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class Guzzle
{

    private $headers = [
        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get($url)
    {
        $response = $this->client->get('https://fantasy.premierleague.com/api/bootstrap-static/', [
            'headers'=> $this->headers
        ]);

        $body = $response->getBody()->getContents();

        Log::info('time6');
        Log::info($response->getStatusCode());
        Log::info($response->getHeaderLine('content-type'));
        Log::info($response->getHeaderLine('content-length'));
        Log::info($url);

        return [
            'body' => $body,
            'status_code' => $response->getStatusCode(),
            'content_type' => $response->getHeaderLine('content-type')
        ];
    }
}
