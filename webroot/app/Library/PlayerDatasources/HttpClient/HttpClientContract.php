<?php
/**
 * Created by PhpStorm.
 * User: albert
 * Date: 9/13/19
 * Time: 7:55 AM
 */

namespace App\Library\PlayerDatasources\HttpClient;


interface HttpClientContract
{
    public function get();

    public function getStatusCode();

    public function getBody();

    public function getHeader();

    public function getContentType();
}