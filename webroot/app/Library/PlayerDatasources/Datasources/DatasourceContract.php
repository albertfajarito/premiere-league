<?php

namespace App\Library\PlayerDatasources\Datasources;

interface DatasourceContract
{
    public function getData();

    public function save();

    public function hasErrors();

    public function getErrors();
}
