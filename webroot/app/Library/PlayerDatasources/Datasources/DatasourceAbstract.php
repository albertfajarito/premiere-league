<?php

namespace App\Library\PlayerDatasources\Datasources;

use App\Library\PlayerDatasources\HttpClient\HttpClientContract;
use http\Client;
use \Illuminate\Support\Facades\Log;

abstract class DatasourceAbstract
{
    protected $httpClientContract;

    protected $data;

    protected $errors;

    protected $lastFetchAttempt;

    protected $model;

    protected $apiResponse;

    protected $playerDataModel;

    protected $requestAttemptLimit; // seconds

    protected $requestAttemptCount = 0;

    protected $requestAttemptCountLimit = 3;

    protected $urlEndpoint;


    public function __construct()
    {
        $this->data = [];
        $this->errors = collect([]);
    }

    public function getData()
    {
        if(count($this->data) == 0){
            $this->setData();
        }

        return $this->data;
    }

    public function hasErrors()
    {
        return $this->errors->isNotEmpty();
    }

    protected function parseData()
    {

        $this->data = collection([]);
    }

    protected function requestDataFromEndPoint()
    {
        $this->resetRequestProperties();

        $this->lastFetchAttempt = time();

        $this->requestAttemptCount += 1;
        $this->apiResponse = $this->httpClientContract->get($this->urlEndpoint);
        try {
            if($this->apiResponse['status_code'] >= 400){
                $this->errors->put('http', "status code: ".$this->apiResponse['status_code']."|".$this->apiResponse['body']);
            }
        } catch (\Exception $e) {
            $this->errors->put('http', $e->getMessage()." ".$this->apiResponse);
        }
    }

    protected function setData()
    {
        $this->requestDataFromEndPoint();
        $this->data = $this->errors->has('http')? [] : $this->unserializeApiResponse();
    }

    protected function unserializeApiResponse()
    {
        $data = [];

        try {
            if(preg_match('/xml/i', $this->apiResponse['content_type'])){
                $data = json_decode(json_encode(simplexml_load_string($this->apiResponse['body'])), true);
            } else if(preg_match('/json/i', $this->apiResponse['content_type'])) {
                $data = json_decode($this->apiResponse['body'], true);
            }
        } catch(\Exception $e) {
            $this->errors->put('parsing', $e->getMessage());
        }

        return $data;
    }

    protected function resetRequestProperties()
    {
        $this->data = [];
        $this->errors = collect([]);
        $this->requestAttemptCount = 0;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getDataModel()
    {
        return $this->playerDataModel;
    }

    public function getHttpClient()
    {
        return $this->httpClientContract;
    }

    public function setFetchedDataModel($playerDataModel)
    {
        $this->playerDataModel = $playerDataModel;
    }

    public function setHttpClient($httpClient)
    {
        $this->httpClientContract = $httpClient;
    }
}
