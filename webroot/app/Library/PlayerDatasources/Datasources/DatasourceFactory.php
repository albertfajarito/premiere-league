<?php

namespace App\Library\PlayerDatasources\Datasources;


use Illuminate\Container\Container;
use Illuminate\Support\Facades\Log;

class DatasourceFactory
{

    protected $datasourceClassNames = [];

    protected $rootNamespace = "";

    public function __construct()
    {
        $this->setRootNamespace();
        $this->setDatasourceClassNames();

    }

    public function create($datasourceClassNames = [])
    {
        if(!is_array($datasourceClassNames)){
            $datasourceClassNames[]=$datasourceClassNames;
        } elseif( count($datasourceClassNames) == 0 ) {
            $datasourceClassNames = $this->datasourceClassNames;
        }

        foreach($datasourceClassNames AS $datasourceClassName){
            if(in_array($datasourceClassName, $this->datasourceClassNames)){
                $datasourceNamespace = $this->rootNamespace."\\".$datasourceClassName;
                $datasourceInstances[] = resolve($datasourceNamespace);
            }
        }
        return collect($datasourceInstances);
    }
    
    protected function setDatasourceClassNames()
    {
       $filesInFolder = \File::files(__DIR__);
       foreach($filesInFolder as $path) {
           $filename = $path->getFilenameWithoutExtension();
           $reflection = new \ReflectionClass($this->rootNamespace."\\".$filename);
           $thisClassPathParts = explode("\\", __CLASS__);
           $thisClassName = end($thisClassPathParts);
           if(!$reflection->isAbstract() && !$reflection->isInterface() && $filename !== $thisClassName){
               $this->datasourceClassNames[] = $path->getFilenameWithoutExtension();
           }
       }
    }

    protected function setRootNamespace()
    {
        $namespaceParts = explode("\\",__NAMESPACE__);
        $this->rootNamespace = implode("\\", $namespaceParts);
    }
}
