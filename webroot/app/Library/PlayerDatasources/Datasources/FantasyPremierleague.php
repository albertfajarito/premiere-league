<?php

namespace App\Library\PlayerDatasources\Datasources;


use Illuminate\Support\Facades\Log;

class FantasyPremierleague extends DatasourceAbstract implements \App\Library\PlayerDatasources\Datasources\DatasourceContract
{
    protected $urlEndpoint = "https://fantasy.premierleague.com/api/bootstrap-static/";

    public function __construct()
    {
        parent::__construct();
    }

    public function save()
    {
        $json_data = json_encode($this->getData());
        return $this->playerDataModel->create([
            'source' => __CLASS__,
            'data' => $json_data
        ]);
    }

    public function getData()
    {
        $data = parent::getData();
        return array_key_exists('elements', $data)? $data['elements'] : [];
    }
}
