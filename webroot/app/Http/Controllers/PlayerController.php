<?php

namespace App\Http\Controllers;


use App\Models\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    protected $player;

    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    public function show($id)
    {
        return $this->player->where('id', $id)->firstOrFail(['id', 'first_name', 'second_name']);
    }

    public function all()
    {
        return $this->player->orderBy('id','asc')->get(['id', 'first_name', 'second_name']);
    }
}
