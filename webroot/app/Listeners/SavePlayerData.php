<?php

namespace App\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SavePlayerData implements ShouldQueue
{
    public $fetchedData;

    public $data;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(count($event->data) > 0 && $this->hasPlayerDataChangedFromPrevious($event->data, $this->getPreviousData())){
            foreach($event->data AS $d){
                \App\Models\Player::updateOrcreate(
                    ['first_name' => $d['first_name'], 'second_name' => $d['second_name']],
                    ['first_name' => $d['first_name'], 'second_name' => $d['second_name']]
                );
            }
        }
    }

    protected function hasPlayerDataChangedFromPrevious($newData, $previousData)
    {
        return hash("crc32", serialize($newData)) !== hash("crc32", serialize($previousData));
    }

    protected function getPreviousData()
    {
        return \App\Models\FetchedPlayerData::latest()->limit(2)->skip(1)->get()->toArray();
    }

}
