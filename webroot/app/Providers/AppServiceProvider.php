<?php

namespace App\Providers;


use App\Library\PlayerDatasources\Datasources\DatasourceAbstract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->resolving(DatasourceAbstract::class, function($object, $app){
           $object->setFetchedDataModel(new \App\Models\FetchedPlayerData());
        });

        $this->app->resolving(DatasourceAbstract::class, function($object, $app){
            $client = config('player.http_client', 'App\Library\PlayerDatasources\HttpClient\Guzzle');
            $object->setHttpClient(new $client());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
