<?php

namespace App\Jobs;

use App\Library\PlayerDatasources\Datasources\DatasourceFactory;
use App\Events\PlayerDataFetched;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class FetchPlayersData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $datasourceFactory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $datasources = new DatasourceFactory();
        $datasources->create()->each(function($datasource, $i){
            $data = $datasource->getData();
            if(!$datasource->hasErrors()){
                $fetchedData = $datasource->save();
                event(new PlayerDataFetched($fetchedData, $data));
            } else {
                Log::info('has errors');
                Log::error(print_r($datasource->getErrors(), true));
            }
        });
    }
}
