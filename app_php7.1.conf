server {
    listen 80;
    listen [::]:80;
    server_name  ucsr-dev.local;

    add_header Strict-Transport-Security "max-age=15768000; includeSubDomains" always;
    add_header X-Frame-Options "DENY";
    add_header X-Content-Type-Options "nosniff";
    add_header X-XSS-Protection "1; mode=block";

    root /var/www/webroot/public;

    index index.php;

    charset utf-8;

    location / {
        try_files $uri /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log /var/log/nginx/ucsr-dev.local-access.error.log;
    error_log  /var/log/nginx/ucsr-dev.local-error.log error;

    client_max_body_size 100m;

    # Files ending in .php will be sent to the PHP-FPM server to be rendered
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    # Allow fonts via cross-region (So Chrome accepts from CDN)
    # Browser cache unchanging files
    location ~* \.(eot|ttc|otf|ttf|woff|woff2|ico|png)$ {
        add_header Access-Control-Allow-Origin *;
        expires 1M;
        access_log off;
        add_header Cache-Control "public";
    }
}
