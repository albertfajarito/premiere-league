#!/usr/bin/env bash
ucsrrepo="git@bitbucket.org:albertfajarito/ultimatecsr-app-fork.git"
webroot_directory=""

cat << "EOF"
==============================================
==                                          ==
==     ██╗   ██╗ ██████╗███████╗██████╗     ==
==     ██║   ██║██╔════╝██╔════╝██╔══██╗    ==
==     ██║   ██║██║     ███████╗██████╔╝    ==
==     ██║   ██║██║     ╚════██║██╔══██╗    ==
==     ╚██████╔╝╚██████╗███████║██║  ██║    ==
==      ╚═════╝  ╚═════╝╚══════╝╚═╝  ╚═╝    ==
==                                          ==
==============================================
==============================================
=                ULTIMATECSR                 =
=          local instance installer          =
==============================================
= Requirements:                              =
=    * Vagrant                               =
=    * VirtualBox                            =
=    * Git                                   =
=                                            =
= Pre-Flight Check                           =
=    * Add your Host computer ssh key to     =
=      your Bitbucket's account              =
=                                            =
= Installer Created By:                      =
=    * Dev Apps Manila                       =
==============================================
==============================================
EOF

echo -e "\e[31m=====Start Requirements Checking=====\e[0m"
echo -e "\e[31m=====Checking Vagrant=====\e[0m"
if ! vagrant --version
then
	echo -e "\e[31mvagrant: not detected\e[0m"
	echo -e "\e[31mmake sure vagrant is installed\e[0m"
	echo -e "\e[31m=====Aborting Deployment=====\e[0m"
	exit
else
	echo -e "vagrant: ok"
fi

echo -e "\e[31m=====Checking VirtualBox=====\e[0m"
if ! vboxmanage --version
then
	echo -e "\e[31mvirtualbox: not detected\e[0m"
	echo -e "\e[31mmake sure virtualbox is installed\e[0m"
	echo -e "\e[31m=====Aborting Deployment=====\e[0m"
	exit
else
	echo -e "virtualbox: ok"
fi

echo -e "\e[31m=====Checking Git=====\e[0m"
if ! git --version
then
	echo -e "\e[31mgit: not detected\e[0m"
	echo -e "\e[31mmake sure git is installed\e[0m"
	echo -e "\e[31m=====Aborting Deployment=====\e[0m"
	exit
else
	echo -e "git: ok"
fi

if [ -d '.vagrant' ]; then
	echo -e "\e[31m=====Existing vagrant deployment detected: Removing old instance=====\e[0m"
	vagrant destroy
	rm -rf .vagrant
fi
echo $webroot_directory
source install_webroot.sh

echo -e "\e[31m=====Start deploying via Vagrant=====\e[0m"
vagrant up
